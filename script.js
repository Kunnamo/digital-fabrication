
function showSection(sectionId) {
    // Handle navigation based on sectionId
    console.log(`Navigating to ${sectionId}`);
}

document.addEventListener('DOMContentLoaded', function () {
    // Parse the URL to get the selected week
    var urlParams = new URLSearchParams(window.location.search);
    var selectedWeek = urlParams.get('week');

    // Hide all sections
    var sections = document.querySelectorAll('.section-container section');
    sections.forEach(function (section) {
        section.style.display = 'none';
    });

    // Show the selected week section
    var selectedSection = document.getElementById(selectedWeek);
    if (selectedSection) {
        selectedSection.style.display = 'block';
    }
});
